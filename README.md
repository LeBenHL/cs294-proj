# README #

Hello CS 294 group. Welcome to our Flask application for Restaurant Inspection predictions with Yelp Data! Read below for some advice for setting up this Flask application

### How do I get set up? ###

## Setting up the virtual environment ##
First you probably want to setup a virtualenv(http://virtualenv.readthedocs.org/en/latest/) for your application. 
Also make sure you have mysql installed (version 5.6).

Once you do so, run:
```
#!shell
 pip install -r requirements.txt
```
to install all the necessary packages for our Flask Application.

## MySQL ##
Our application uses MySQL as our backing store. The application expects that a database named datasf_yelp can be accessed with username:datasf_yelp and no password. Please create the database before running the Flask Applcation.

## Database Migrations ##
Revisions to the database is done using alembic(http://alembic.readthedocs.org/en/latest/). Run
```
#!shell
alembic upgrade head
```
in the root directory of the application in order to upgrade the database to its latest state.

## Loading SF Health Inspection Data ##
SF Data has been pre-cleaned/pre-loaded in the data/datasf folder in the root directory. In order to load the data into the database, you can use the manage.py script however. Run
```
#!shell
python manage.py load_sf_data
```
in the root directory to load all the SF data into the appropriate tables.

## Loading LV Heath Inspection Data ##
LV Restaurant and Inspections data has been scraped and stored in data/scraping in the root directory courtesy of Jefferson Lai. In order to load the data into the database, you can use the manage.py script Run

```
#!shell
python manage.py load_lv_data
```
in the root directory to load the LV data into the appropriate table.

## Loading Yelp Data ##
We are using Yelp Data found here for the LV Restaurants http://www.yelp.com/dataset_challenge. In order to load this data into the DB, run this command in the root directory of the app.
```
#!shell
python manage.py load_yelp_data -d {location of json file you want to load in}
```
This dataset is quite large so be patient as it will take a long time to load in all the data.

## Entity Resolving SF Restaurants with Yelp Restaurants ##
If you look at the SF Restaurants table, there are currently empty fields for Yelp data. To fill in those fields using the Yelp Search API run
```
#!shell
python manage.py entity_resolve_sf_restaurants_with_yelp
```
in the root directory. This command also takes optional flags -n {#} (The number of restaurants you want to resolve) and -r (Whether or not to reresolve restaurants that already have associated Yelp Data cached in the databse)
```
#!shell
python manage.py entity_resolve_restaurants_with_yelp -n 10 #Resolve next 10 restaurants
python manage.py entity_resolve_restaurants_with_yelp -n 10 -r #Reresolve the first 10 restaurants in the database.
```

## Entity Resolving LV Restaurants with Yelp Restaurants ##
The code for entity resolution can be found in the iPython notebook Entity Resolution.ipynb.

This notebook will output a CSV file containing (lv_restaurant_id,yelp_business_id) pairs. The format of the CSV file is same_entities_f_{number}.csv where "number" is the beta used in the Fbeta score when scoring our classifier. Lower numbers of beta means we value precision in our entity resolution more than recall during entity resolution.

In order to load the CSV same entity file into our database, run this command in the root directory.

```
#!shell
python manage.py load_yelp_lv_restaurants_equivalents -d {location of the same entities CSV file}
```

## Loading Final Predictions ##
When we finally have predicted inspection grades for restaurants, lets load them into the database. We expect a CSV file of (yelp_business_id, predicted_A_grade) pairs.

Run this command in the root directory

```
#!shell
python manage.py load_predictions -d {location of the predictions CSV file}
```


## MaltParser ##

At the suggestion of Professor Canny, we will probably use the Malt Parser for parsing dependencies within our sentences (http://www.maltparser.org/). Python NLTK hooks up with the Java implementation of the Malt Parser luckily for our application (http://www.nltk.org/_modules/nltk/parse/malt.html). In order to use NLTK Malt Parser, you will need to set an environment variable pointing the the directory with the Malt Parser jar and download files from NLTK. To do so within python, run these commands.

```
#!python
os.environ["MALT_PARSER"] = "./maltparser-1.8"
nltk.download('all')
```

We will also most likely use a pretrained model when parsing our reviews. Pretrained models can be found here (http://www.maltparser.org/mco/english_parser/engmalt.html). We will probably use the linear parser as it is faster.

## Running the Flask Application ##
Using the manage.py script run
```
#!shell
python manage.py runserver -p {port no.}
```
to get the application running on localhost on the given port.

## Installing additional python packages ##
When pip installing more packages or newer versions of packages.. Please do not forget to 
```
#!shell
pip freeze > requirements.txt
```
To freeze the packages. This way, everyone is always using the same environment at all times.

## Flask-Bootstrap ##
We are using Flask-Bootstrap (https://github.com/mbr/flask-bootstrap) to integrate Twitter Bootstrap into our views. This maybe more powerful of a tool than we need as perhaps
it is better to just install Bootstrap files ourselves but this is experiementation and for fun for now.

## Useful links ##
* Flask - http://flask.pocoo.org/
* Flask-Bootstrap - https://github.com/mbr/flask-bootstrap
* Flask-SQLAlchemy - https://pythonhosted.org/Flask-SQLAlchemy/
* Flask-Script - http://flask-script.readthedocs.org/en/latest/
* Alembic - http://alembic.readthedocs.org/en/latest/
* Python-Levenshtein - http://www.coli.uni-saarland.de/courses/LT1/2011/slides/Python-Levenshtein.html
* SQLAlchemy - http://www.sqlalchemy.org/
* virtualenv - http://virtualenv.readthedocs.org/en/latest/