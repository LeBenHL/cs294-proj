from wtforms.form import Form
from wtforms.ext.sqlalchemy.fields import QuerySelectField

from datasf_yelp.models import YelpBusiness

class YelpRestaurantSelectorForm(Form):
    yelp_restaurants = QuerySelectField(query_factory=lambda: YelpBusiness.query.filter(YelpBusiness.predicted_A_grade != None).order_by(YelpBusiness.name),
                                        get_label='name',
                                        allow_blank=True,
                                        blank_text=u'')