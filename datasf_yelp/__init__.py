from flask import Flask
from flask_bootstrap import Bootstrap
from flask.ext.sqlalchemy import SQLAlchemy

app = Flask(__name__)
app.config.from_pyfile('../config.py', silent=True)
Bootstrap(app)
db = SQLAlchemy(app)

from datasf_yelp.views import HomeView, PredictionsView

PredictionsView.register(app)
HomeView.register(app, route_base='/')
