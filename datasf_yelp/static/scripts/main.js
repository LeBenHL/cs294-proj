$(document).ready(function($) {
  $(".chosen-select").chosen();

  $(".chosen-select").change(function() {
  	var url = $(this).data("url") + $(this).val();
  	$.ajax(url)
  	.done(function(item) {
  		if (item.predicted_A_grade) {
  			$('#result-message').html('<h2 class="bg-success text-center"> This business follows safety codes and is safe to eat at!</h2>')
  			$('#result-img').html('<img class="center-block" src="static/images/check.png" />');
  		} else {
  			$('#result-message').html('<h2 class="bg-danger text-center"> This business may not follow the strictest health standards.</h2>')
  			$('#result-img').html('<img class="center-block" src="static/images/x.png" />');
  		}
  	})
  	.fail(function() {
  		$('#result-message').html('<h2 class="bg-warning">Sorry something went wrong.</h2>');
  	});
	})
});