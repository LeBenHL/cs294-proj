from Levenshtein import ratio

NAME_SIMILARITY_WEIGHT = 1
ADDRESS_SIMILARITY_WEIGHT = 1

def best_match(restaurant, yelp_businesses):
    """
    Returns the best yelp business and its similarity values to the sf restaurant by calculating a
    weighted sum of the string similarities of the name of the restaurant and the street address between
    our data and the data on yelp. The best yelp business is the one with the highest weighted similarity value.
    """

    best_match = None
    best_match_address_similarity = 0
    best_match_name_similarity = 0
    best_match_weighted_similarity = 0

    restaurant_address = restaurant.address.lower()
    restaurant_name = restaurant.name.lower()

    for yelp_business in yelp_businesses:
        address = " ".join(yelp_business['location']['address']).lower()
        name = yelp_business['name'].lower()

        if address:
            address_similarity = ratio(restaurant_address, address)
        else:
            address_similarity = 0
        if name:
            name_similarity = ratio(restaurant_name, name)
        else:
            name_similarity = 0

        weighted_similarity = NAME_SIMILARITY_WEIGHT * name_similarity + ADDRESS_SIMILARITY_WEIGHT * address_similarity

        if weighted_similarity >= best_match_weighted_similarity:
            best_match = yelp_business
            best_match_address_similarity = address_similarity
            best_match_name_similarity = name_similarity
            best_match_weighted_similarity = weighted_similarity

    return best_match, best_match_address_similarity, best_match_name_similarity, best_match_weighted_similarity