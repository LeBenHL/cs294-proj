from itertools import chain
import re
import requests
from requests_oauthlib import OAuth1

from datasf_yelp import app, db
from datasf_yelp.columns import JSONEncoded, JSONEncodedText
from datasf_yelp.utils import best_match

class SFRestaurant(db.Model):
    __tablename__ = "sf_restaurants"

    YELP_SEARCH_API_URL = "http://api.yelp.com/v2/search"

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(100))
    address = db.Column(db.String(100))
    city = db.Column(db.String(50))
    postal_code = db.Column(db.String(20))
    latitude = db.Column(db.Float)
    longitude = db.Column(db.Float)
    phone_number = db.Column(db.String(30))
    tax_code = db.Column(db.String(20))
    business_certificate = db.Column(db.Integer)
    application_date = db.Column(db.Date)
    owner_name = db.Column(db.String(100))
    owner_address = db.Column(db.String(100))
    owner_city = db.Column(db.String(50))
    owner_state = db.Column(db.String(20))
    owner_zip = db.Column(db.String(20))
    
    # YELP FIELDS
    yelp_id = db.Column(db.String(100))
    yelp_name = db.Column(db.String(100))
    yelp_url = db.Column(db.String(100))
    yelp_rating = db.Column(db.Float)
    yelp_address = db.Column(db.String(250))
    address_similarity = db.Column(db.Float)
    name_similarity = db.Column(db.Float)

    def find_yelp_equivalent(self):
        """
        Uses the Yelp Search API in order to find the equivalent restaurant on Yelp and returns the JSON response
        """
        term = re.sub(r'#\d+', r'', self.name)
        relevance_params = {'term': term, 'sort': 0, # Sort 0 sorts by relevance
                  'll': '%s,%s' % (self.latitude, self.longitude), 'limit': 10}
        distance_params = {'term': term, 'sort': 1, # Sort 1 sorts by distance
                  'll': '%s,%s' % (self.latitude, self.longitude), 'limit': 10}
        auth = OAuth1(app.config['YELP_APP_KEY'], app.config['YELP_APP_SECRET'], app.config['YELP_AUTH_TOKEN'], app.config['YELP_SECRET_TOKEN'])

        # First, find the best match out of restaurants returned by the relevance search
        try:
            relevant_businesses = requests.get(self.YELP_SEARCH_API_URL, auth=auth, params=relevance_params).json().get('businesses', [])
            close_businesses = requests.get(self.YELP_SEARCH_API_URL, auth=auth, params=distance_params).json().get('businesses', [])
            return best_match(self, chain(relevant_businesses, close_businesses))
        except KeyError:
            return None, None, None, None


class SFInspection(db.Model):
    __tablename__ = "sf_inspections"

    id = db.Column(db.Integer, primary_key=True)
    sf_restaurant_id = db.Column(db.Integer)
    score = db.Column(db.Integer)
    date = db.Column(db.Date)
    type = db.Column(db.String(50))


class SFViolation(db.Model):
    __tablename__ = "sf_violations"

    id = db.Column(db.Integer, primary_key=True)
    sf_restaurant_id = db.Column(db.Integer)
    date = db.Column(db.Date)
    violation_type_id = db.Column(db.Integer)
    risk_category = db.Column(db.String(30))
    description = db.Column(db.String(250))
    
class LVRestaurant(db.Model):
    __tablename__ = "lv_restaurants"

    id = db.Column(db.Integer, primary_key=True)
    restaurant_name = db.Column(db.String(100))
    category_name = db.Column(db.String(100))
    current_grade = db.Column(db.String(20))
    date_current = db.Column(db.Date)
    insp_type = db.Column(db.String(100))
    address = db.Column(db.String(100))
    city_name = db.Column(db.String(50))
    state = db.Column(db.String(20))
    zip_code = db.Column(db.String(20))
    latitude = db.Column(db.Float)
    longitude = db.Column(db.Float)
    permit_id = db.Column(db.String(30))
    permit_number = db.Column(db.String(30))
    demerits = db.Column(db.Integer)
    violations = db.Column(db.String(200))

    prev_inspections = db.relationship('LVPrevInspection',
                                       primaryjoin="LVRestaurant.id==LVPrevInspection.restaurant_id",
                                       foreign_keys='LVPrevInspection.restaurant_id', order_by="LVPrevInspection.inspection_date", backref="restaurant")

    yelp_businesses = db.relationship("YelpBusiness", secondary="yelp_lv_restaurants_equivalents", backref="lv_restaurants")

    def inspection_grade_associated_reviews_iter(self):
        left_window = None
        right_window = None

        for prev_inspection in self.prev_inspections:
            left_window = right_window
            right_window = prev_inspection.inspection_date
            reviews = YelpReview.query.join(YelpReview.business, YelpBusiness.lv_restaurants).filter(LVRestaurant.id == self.id)
            if left_window:
                reviews = reviews.filter(YelpReview.date >= left_window)
            if right_window:
                reviews = reviews.filter(YelpReview.date < right_window)

            yield left_window, right_window, prev_inspection.inspection_grade, reviews

        # Time Window including current inspection grade
        left_window = right_window
        right_window = None
        reviews = YelpReview.query.join(YelpReview.business, YelpBusiness.lv_restaurants).filter(LVRestaurant.id == self.id)
        if left_window:
            reviews = reviews.filter(YelpReview.date >= left_window)
        if right_window:
            reviews = reviews.filter(YelpReview.date < right_window)
        yield left_window, right_window, self.current_grade, reviews


class LVPrevInspection(db.Model):
    __tablename__ = "lv_prev_inspections"
    
    id = db.Column(db.Integer, primary_key=True)
    restaurant_id = db.Column(db.Integer)
    inspection_demerits = db.Column(db.Integer)
    inspection_grade = db.Column(db.String(20))
    permit_status = db.Column(db.String(30))
    permit_id = db.Column(db.String(30))
    inspection_time = db.Column(db.DateTime)
    inspection_id = db.Column(db.String(30))
    violations = db.Column(db.String(200))
    inspection_type = db.Column(db.String(50))
    inspection_date = db.Column(db.Date)


class YelpBusiness(db.Model):
    __tablename__ = "yelp_businesses"

    business_id = db.Column(db.String(100), primary_key=True)
    name = db.Column(db.String(100))
    neighborhoods = db.Column(JSONEncoded(1000))
    full_address = db.Column(db.String(200))
    city = db.Column(db.String(100))
    state = db.Column(db.String(50))
    latitude = db.Column(db.Float)
    longitude = db.Column(db.Float)
    stars = db.Column(db.Float)
    review_count = db.Column(db.Integer)
    categories = db.Column(JSONEncoded(1000))
    open = db.Column(db.Boolean)
    hours = db.Column(JSONEncoded(1000))
    attributes = db.Column(JSONEncoded(1500))

    # Column we store our predictions in
    predicted_A_grade = db.Column(db.Boolean)

    tips = db.relationship("YelpTip", primaryjoin="YelpBusiness.business_id==YelpTip.business_id", foreign_keys='YelpTip.business_id', backref='business')
    reviews = db.relationship("YelpReview", primaryjoin="YelpBusiness.business_id==YelpReview.business_id", foreign_keys='YelpReview.business_id', backref='business')
    checkins = db.relationship("YelpCheckin", primaryjoin="YelpBusiness.business_id==YelpCheckin.business_id", foreign_keys='YelpCheckin.business_id', backref='business')

    reviews_query = db.relationship("YelpReview", primaryjoin="YelpBusiness.business_id==YelpReview.business_id", foreign_keys='YelpReview.business_id', lazy="dynamic")


class YelpReview(db.Model):
    __tablename__ = "yelp_reviews"
    
    review_id = db.Column(db.String(100), primary_key=True)
    business_id = db.Column(db.String(100))
    user_id = db.Column(db.String(100))
    stars = db.Column(db.Float)
    text = db.Column(db.Text)
    date = db.Column(db.Date)
    votes = db.Column(JSONEncoded(1000))


class YelpUser(db.Model):
    __tablename__ = "yelp_users"
    
    user_id = db.Column(db.String(100), primary_key=True)
    name = db.Column(db.String(100))
    review_count = db.Column(db.Integer)
    average_stars = db.Column(db.Float)
    votes = db.Column(JSONEncoded(1000))
    elite = db.Column(JSONEncoded(1000))
    yelping_since = db.Column(db.String(100))
    compliments = db.Column(JSONEncodedText)
    fans = db.Column(db.Integer)

    tips = db.relationship("YelpTip", primaryjoin="YelpUser.user_id==YelpTip.user_id", foreign_keys='YelpTip.user_id', backref='user')
    reviews = db.relationship("YelpReview", primaryjoin="YelpUser.user_id==YelpReview.user_id", foreign_keys='YelpReview.user_id', backref='user')


class YelpCheckin(db.Model):
    __tablename__ = "yelp_checkins"

    checkin_id = db.Column(db.Integer, primary_key=True)
    business_id = db.Column(db.String(100))
    checkin_info = db.Column(JSONEncoded(2500))


class YelpTip(db.Model):
    __tablename__ = "yelp_tips"
    
    tip_id = db.Column(db.Integer, primary_key=True)
    text = db.Column(db.Text)
    business_id = db.Column(db.String(100))
    user_id = db.Column(db.String(100))
    date = db.Column(db.Date)
    likes = db.Column(db.Integer)

    
class YelpLVRestaurantSimilarity(db.Model):
    __tablename__ = "yelp_lv_restaurants_similarities"

    lv_restaurant_id = db.Column(db.Integer, db.ForeignKey('lv_restaurants.id'), primary_key=True)
    yelp_business_id = db.Column(db.String(100), db.ForeignKey('yelp_businesses.business_id'), primary_key=True)
    name_ratio_score = db.Column(db.Float)
    name_jaro_score = db.Column(db.Float)
    address_ratio_score = db.Column(db.Float)
    address_jaro_score = db.Column(db.Float)
    latitude_delta = db.Column(db.Float)
    longitude_delta = db.Column(db.Float)
    is_same_entity = db.Column(db.Boolean)

    lv_restaurant = db.relation("LVRestaurant", backref="similarities")
    yelp_business = db.relationship("YelpBusiness", backref="similarities")


class YelpLVRestaurantEquivalent(db.Model):
    __tablename__ = "yelp_lv_restaurants_equivalents"

    lv_restaurant_id = db.Column(db.Integer, db.ForeignKey('lv_restaurants.id'), primary_key=True)
    yelp_business_id = db.Column(db.String(100), db.ForeignKey('yelp_businesses.business_id'), primary_key=True)

    lv_restaurant = db.relation("LVRestaurant", backref="equivalents")
    yelp_business = db.relationship("YelpBusiness", backref="equivalents")
