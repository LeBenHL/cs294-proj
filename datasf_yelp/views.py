from flask import abort, jsonify, render_template
from flask.ext.classy import FlaskView

from datasf_yelp.forms import YelpRestaurantSelectorForm
from datasf_yelp.models import YelpBusiness

class HomeView(FlaskView):

    def index(self):
        selectorForm = YelpRestaurantSelectorForm()
        return render_template('index.html', form=selectorForm)


class PredictionsView(FlaskView):

    def index(self):
        yelp_businesses = YelpBusiness.query.filter(YelpBusiness.predicted_A_grade != None)\
                                            .order_by(YelpBusiness.predicted_A_grade, YelpBusiness.name)
        return render_template('predictions_index.html', yelp_businesses=yelp_businesses)

    def get(self, id):
        yelp_restaurant = YelpBusiness.query.get(id)
        if not yelp_restaurant or yelp_restaurant.predicted_A_grade is None:
            abort(404)
        return jsonify({'name': yelp_restaurant.name,
                        'full_address': yelp_restaurant.full_address,
                        'predicted_A_grade': yelp_restaurant.predicted_A_grade})
