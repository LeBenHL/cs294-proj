from sqlalchemy.types import TypeDecorator, VARCHAR, TEXT
import simplejson as json

class JSONEncoded(TypeDecorator):
    """Represents an immutable structure as a json-encoded string.

    Usage::

        JSONEncoded(255)

    """

    impl = VARCHAR

    def process_bind_param(self, value, dialect):
        if value is not None:
            value = json.dumps(value)

        return value.strip('"')

    def process_result_value(self, value, dialect):
        if value is not None:
            value = json.loads(value)
        return value

class JSONEncodedText(TypeDecorator):
    """Represents an immutable structure as a json-encoded string.

    Usage::

        JSONEncoded(255)

    """

    impl = TEXT

    def process_bind_param(self, value, dialect):
        if value is not None:
            value = json.dumps(value)

        return value

    def process_result_value(self, value, dialect):
        if value is not None:
            value = json.loads(value)
        return value