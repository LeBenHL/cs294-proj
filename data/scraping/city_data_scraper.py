#!/usr/bin/python

import csv
import sys
import requests
from bs4 import BeautifulSoup

# Return string form of each field as a list
def get_column_string(td):
  if td.string:
    return td.string.encode('utf-8')
  elif td.find('div', {'class': 'br_stars'}):
    return td.find('div', {'class': 'br_stars'})['title'].encode('utf-8')
  else:
    return 'n/a'

def get_ratings_from_page(url):
  print 'Attempting to get ratings from "%s"' % url
  response = requests.get(url)
  ascii_response_text = response.text.encode('utf-8')
  soup = BeautifulSoup(ascii_response_text)

  rating_table = soup.body.find('table', {'class': 'restTAB'}).find('tbody')
  
  page_ratings = []
  for row in rating_table.find_all('tr'):
    page_ratings.append(map(get_column_string, row.find_all('td')))
  return page_ratings



if __name__ == '__main__':
  if len(sys.argv) > 1:
    out_file = sys.argv[1]
  else:
    out_file = 'restaurant_scores.csv'
  
  with open(out_file, 'w+') as f:
    writer = csv.writer(f)
    header = ["name", "rating", "address", "city", "zipcode", "insp", "last_inspection_date", "grade"]
    ratings = []
    writer.writerow(header)
    for i in range(1,26):
      url = 'http://www.city-data.com/las-vegas-restaurants/index%s.html' % i
      page_ratings = get_ratings_from_page(url)
      ratings += page_ratings
    ratings.sort(key=lambda rating: rating[0])
    writer.writerows(ratings)
