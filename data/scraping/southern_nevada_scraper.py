#!/usr/bin/python

import sys
import simplejson as json
import csv
import requests
from bs4 import BeautifulSoup


URL = 'http://southernnevadahealthdistrict.org/restaurants/stores/restaurants.php'
HEADER =  ['restaurant_name', 'category_name', 'current_grade', 'date_current', 'insp_type', 'address', 'city_name', 'state', 'zip_code', 'latitude', 'longitude', 'prev_insp', 'permit_id', 'permit_number', 'demerits', 'violations']

# Return a list of dictionaries, each representing a restaurant
def get_restaurants(start=0, limit=20):
  payload = {'start': str(start), 'limit': str(limit)}
  response = requests.post(URL, data=payload)
  json_string = str(response.text).replace('total', '"total"', 1).replace('restaurants', '"restaurants"', 1)
  restaurants_json = json.loads(json_string)
  return restaurants_json['restaurants'] 

def get_row_from_restaurant(restaurant_dict, fill_value='N/A'):
  row = []
  for col in HEADER:
    try:
      row.append(str(restaurant_dict[col]))
    except KeyError:
      print 'Unable to find column "%s" in restaurant: %s, filling with %s' % (col, restaurant_dict['restaurant_name'], fill_value)
      row.append(fill_value)
  return row

def write_to_csv(out_file, extension='.csv'):
  with open(out_file+extension, 'w+') as f:
    writer = csv.writer(f)
    writer.writerow(HEADER)
    next_restaurant_idx = 0
    next_restaurants = get_restaurants(next_restaurant_idx)
    while len(next_restaurants) > 0:
      print next_restaurant_idx
      for restaurant in next_restaurants:
        writer.writerow(get_row_from_restaurant(restaurant))
      next_restaurant_idx += 20
      next_restaurants = get_restaurants(next_restaurant_idx)

def write_to_json(out_file, extension='.json'):
  with open(out_file+extension, 'w+') as f:
    next_restaurant_idx = 0
    next_restaurants = get_restaurants(next_restaurant_idx)
    restaurants = next_restaurants
    while len(next_restaurants) > 0:
      print next_restaurant_idx
      next_restaurant_idx += 20
      next_restaurants = get_restaurants(next_restaurant_idx)
      restaurants += next_restaurants
    json.dump(restaurants, f)


if __name__ == "__main__":

  if len(sys.argv) > 2:
    out_file = sys.argv[2]
  else:
    out_file = 'southern_nevada_scores'

  if len(sys.argv) < 2:
    print "Please specify an output type (csv or json)"
  elif sys.argv[1] == 'csv':
    write_to_csv(out_file)
  elif sys.argv[1] == 'json':
    write_to_json(out_file)
  else:
    print "Invalid output format provided"

      
      
