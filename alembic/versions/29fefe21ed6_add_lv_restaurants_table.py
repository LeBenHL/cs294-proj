"""Add LV Restaurants Table

Revision ID: 29fefe21ed6
Revises: 2fa461c3785a
Create Date: 2014-10-22 21:17:51.087412

"""

# revision identifiers, used by Alembic.
revision = '29fefe21ed6'
down_revision = '2fa461c3785a'

from alembic import op
import sqlalchemy as sa


def upgrade():
     op.create_table('lv_restaurants',
                    sa.Column('id', sa.Integer, primary_key=True),
                    sa.Column('name', sa.String(100)),
                    sa.Column('rating', sa.String(100)),
                    sa.Column('address', sa.String(150)),
                    sa.Column('city', sa.String(50)),
                    sa.Column("zipcode", sa.String(20)),
                    sa.Column("insp", sa.Integer),
                    sa.Column("last_inspection_date", sa.Date),
                    sa.Column("grade", sa.String(10))
    )


def downgrade():
    op.drop_table('lv_restaurants')
