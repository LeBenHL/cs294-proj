"""Add dataSF tables

Revision ID: 45e7d9382815
Revises: None
Create Date: 2014-10-16 13:59:47.068310

"""

# revision identifiers, used by Alembic.
revision = '45e7d9382815'
down_revision = None

from alembic import op
import sqlalchemy as sa

def upgrade():
    op.create_table('sf_restaurants',
                        sa.Column('id', sa.Integer, primary_key=True),
                        sa.Column('name', sa.String(100)),
                        sa.Column('address', sa.String(100)),
                        sa.Column('city', sa.String(50)),
                        sa.Column('postal_code', sa.String(20)),
                        sa.Column('latitude', sa.Float),
                        sa.Column('longitude', sa.Float),
                        sa.Column('phone_number', sa.String(30)),
                        sa.Column('tax_code', sa.String(20)),
                        sa.Column('business_certificate', sa.Integer),
                        sa.Column('application_date', sa.Date),
                        sa.Column('owner_name', sa.String(100)),
                        sa.Column('owner_address', sa.String(100)),
                        sa.Column('owner_city', sa.String(50)),
                        sa.Column('owner_state', sa.String(20)),
                        sa.Column('owner_zip', sa.String(20))
                        )
    op.create_table('sf_inspections',
                    sa.Column('id', sa.Integer, primary_key=True),
                    sa.Column('sf_restaurant_id', sa.Integer),
                    sa.Column('score', sa.Integer),
                    sa.Column('date', sa.Date),
                    sa.Column('type', sa.String(50))
                   )
    op.create_table('sf_violations',
                    sa.Column('id', sa.Integer, primary_key=True),
                    sa.Column('sf_restaurant_id', sa.Integer),
                    sa.Column('date', sa.Date),
                    sa.Column('violation_type_id', sa.Integer),
                    sa.Column('risk_category', sa.String(30)),
                    sa.Column('description', sa.String(250))
                   )


def downgrade():
    op.drop_table('sf_violations')
    op.drop_table('sf_inspections')
    op.drop_table('sf_restaurants')
