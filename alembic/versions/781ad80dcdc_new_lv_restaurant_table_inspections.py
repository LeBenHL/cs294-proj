"""New LV Restaurant Table + Inspections

Revision ID: 781ad80dcdc
Revises: 2be251c7a14c
Create Date: 2014-10-23 02:29:49.996901

"""

# revision identifiers, used by Alembic.
revision = '781ad80dcdc'
down_revision = '2be251c7a14c'

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.drop_table('lv_restaurants')
    op.create_table('lv_restaurants',
                    sa.Column('id', sa.Integer, primary_key=True),
                    sa.Column('restaurant_name', sa.String(100)),
                    sa.Column('category_name', sa.String(100)),
                    sa.Column("current_grade", sa.String(20)),
                    sa.Column("date_current", sa.Date),
                    sa.Column("insp_type", sa.String(100)),
                    sa.Column("address", sa.String(100)),
                    sa.Column("city_name", sa.String(50)),
                    sa.Column("state", sa.String(20)),
                    sa.Column("zip_code", sa.String(20)),
                    sa.Column("latitude", sa.Float),
                    sa.Column("longitude", sa.Float),
                    sa.Column("permit_id", sa.String(30)),
                    sa.Column("permit_number", sa.String(30)),
                    sa.Column("demerits", sa.Integer),
                    sa.Column("violations", sa.String(200))
    )
    op.create_table('lv_prev_inspections',
                    sa.Column('id', sa.Integer, primary_key=True),
                    sa.Column('restaurant_id', sa.Integer),
                    sa.Column("inspection_demerits", sa.Integer),
                    sa.Column("inspection_grade", sa.String(20)),
                    sa.Column("permit_status", sa.String(30)),
                    sa.Column("permit_id", sa.String(30)),
                    sa.Column("inspection_time", sa.DateTime),
                    sa.Column("inspection_id", sa.String(30)),
                    sa.Column("violations", sa.String(200)),
                    sa.Column("inspection_type", sa.String(50)),
                    sa.Column("inspection_date", sa.Date),
    )


def downgrade():
    op.drop_table('lv_restaurants')
    op.drop_table('lv_prev_inspections')
    op.create_table('lv_restaurants',
                    sa.Column('id', sa.Integer, primary_key=True),
                    sa.Column('name', sa.String(100)),
                    sa.Column('rating', sa.String(100)),
                    sa.Column('address', sa.String(150)),
                    sa.Column('city', sa.String(50)),
                    sa.Column("zipcode", sa.String(20)),
                    sa.Column("insp", sa.Integer),
                    sa.Column("last_inspection_date", sa.Date),
                    sa.Column("grade", sa.String(10))
    )
