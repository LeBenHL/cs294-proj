"""Add is_same_entity column to similarity table

Revision ID: 1ac6a1c015c2
Revises: 4882840df93e
Create Date: 2014-10-29 14:59:41.959841

"""

# revision identifiers, used by Alembic.
revision = '1ac6a1c015c2'
down_revision = '4882840df93e'

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.add_column('yelp_lv_restaurants_similarities', sa.Column('is_same_entity', sa.Boolean))


def downgrade():
    op.drop_column('yelp_lv_restaurants_similarities', 'is_same_entity')
