"""Add LV restaurants/Yelp Restaurants Similarity Table

Revision ID: 4882840df93e
Revises: 2e8dfabeb9eb
Create Date: 2014-10-28 21:37:26.390015

"""

# revision identifiers, used by Alembic.
revision = '4882840df93e'
down_revision = '2e8dfabeb9eb'

from alembic import op
import sqlalchemy as sa


def upgrade():
     op.create_table('yelp_lv_restaurants_similarities',
                    sa.Column('lv_restaurant_id', sa.Integer, sa.ForeignKey('lv_restaurants.id'), primary_key=True),
                    sa.Column("yelp_business_id", sa.String(100), sa.ForeignKey('yelp_businesses.business_id'), primary_key=True),
                    sa.Column("name_ratio_score", sa.Float),
                    sa.Column("name_jaro_score", sa.Float),
                    sa.Column("address_ratio_score", sa.Float),
                    sa.Column("address_jaro_score", sa.Float),
                    sa.Column("latitude_delta", sa.Float),
                    sa.Column("longitude_delta", sa.Float)
    )


def downgrade():
    op.drop_table('yelp_lv_restaurants_similarities')
