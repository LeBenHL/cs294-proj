"""app_yelp_columns_to_sf_restaurants

Revision ID: 2fa461c3785a
Revises: 45e7d9382815
Create Date: 2014-10-17 01:48:43.086936

"""

# revision identifiers, used by Alembic.
revision = '2fa461c3785a'
down_revision = '45e7d9382815'

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.add_column('sf_restaurants', sa.Column('yelp_id', sa.String(100)))
    op.add_column('sf_restaurants', sa.Column('yelp_name', sa.String(100)))
    op.add_column('sf_restaurants', sa.Column('yelp_url', sa.String(100)))
    op.add_column('sf_restaurants', sa.Column('yelp_rating', sa.Float))
    op.add_column('sf_restaurants', sa.Column('yelp_address', sa.String(250)))
    op.add_column('sf_restaurants', sa.Column('address_similarity', sa.Float))
    op.add_column('sf_restaurants', sa.Column('name_similarity', sa.Float))


def downgrade():
    op.drop_column('sf_restaurants', 'yelp_id')
    op.drop_column('sf_restaurants', 'yelp_name')
    op.drop_column('sf_restaurants', 'yelp_url')
    op.drop_column('sf_restaurants', 'yelp_rating')
    op.drop_column('sf_restaurants', 'yelp_address')
    op.drop_column('sf_restaurants', 'address_similarity')
    op.drop_column('sf_restaurants', 'name_similarity')
