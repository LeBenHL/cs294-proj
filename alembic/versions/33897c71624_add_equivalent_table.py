"""Add Equivalent Table

Revision ID: 33897c71624
Revises: 1ac6a1c015c2
Create Date: 2014-11-01 02:10:50.042509

"""

# revision identifiers, used by Alembic.
revision = '33897c71624'
down_revision = '1ac6a1c015c2'

from alembic import op
import sqlalchemy as sa


def upgrade():
     op.create_table('yelp_lv_restaurants_equivalents',
                    sa.Column('lv_restaurant_id', sa.Integer, sa.ForeignKey('lv_restaurants.id'), primary_key=True),
                    sa.Column("yelp_business_id", sa.String(100), sa.ForeignKey('yelp_businesses.business_id'), primary_key=True),
    )


def downgrade():
    op.drop_table('yelp_lv_restaurants_equivalents')
