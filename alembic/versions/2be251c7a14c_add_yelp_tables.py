"""Add Yelp Tables

Revision ID: 2be251c7a14c
Revises: 29fefe21ed6
Create Date: 2014-10-22 23:57:23.442661

"""

# revision identifiers, used by Alembic.
revision = '2be251c7a14c'
down_revision = '29fefe21ed6'

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.create_table('yelp_businesses',
                    sa.Column('business_id', sa.String(100), primary_key=True),
                    sa.Column('name', sa.String(100)),
                    sa.Column('neighborhoods', sa.String(1000)),
                    sa.Column('full_address', sa.String(200)),
                    sa.Column('city', sa.String(100)),
                    sa.Column('state', sa.String(50)),
                    sa.Column('latitude', sa.Float),
                    sa.Column('longitude', sa.Float),
                    sa.Column('stars', sa.Float),
                    sa.Column('review_count', sa.Integer),
                    sa.Column('categories', sa.String(1000)),
                    sa.Column('open', sa.Boolean),
                    sa.Column('hours', sa.String(1000)),
                    sa.Column('attributes', sa.String(1500)),
    )
    op.create_table('yelp_reviews',
                    sa.Column('review_id', sa.String(100), primary_key=True),
                    sa.Column('business_id', sa.String(100)),
                    sa.Column('user_id', sa.String(100)),
                    sa.Column('stars', sa.Float),
                    sa.Column('text', sa.Text),
                    sa.Column('date', sa.Date),
                    sa.Column('votes', sa.String(1000)),
    )
    op.create_table('yelp_users',
                    sa.Column('user_id', sa.String(100), primary_key=True),
                    sa.Column('name', sa.String(100)),
                    sa.Column('review_count', sa.Integer),
                    sa.Column('average_stars', sa.Float),
                    sa.Column('votes', sa.String(1000)),
                    sa.Column('elite', sa.String(1000)),
                    sa.Column('yelping_since', sa.String(100)),
                    sa.Column('compliments', sa.String(1000)),
                    sa.Column('fans', sa.Integer),
    )
    op.create_table('yelp_checkins',
                    sa.Column('checkin_id', sa.Integer, primary_key=True),
                    sa.Column('business_id', sa.String(100)),
                    sa.Column('checkin_info', sa.Text),
    )
    op.create_table('yelp_tips',
                    sa.Column('tip_id', sa.Integer, primary_key=True),
                    sa.Column('text', sa.Text),
                    sa.Column('business_id', sa.String(100)),
                    sa.Column('user_id', sa.String(100)),
                    sa.Column('date', sa.Date),
                    sa.Column("likes", sa.Integer),
    )


def downgrade():
    op.drop_table('yelp_businesses')
    op.drop_table('yelp_reviews')
    op.drop_table('yelp_users')
    op.drop_table('yelp_checkins')
    op.drop_table('yelp_tips')