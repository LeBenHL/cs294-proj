"""Create Indices for Yelp business ids fks, Yelp user ids fks, and LV restaurant id fk's

Revision ID: 2e8dfabeb9eb
Revises: 781ad80dcdc
Create Date: 2014-10-23 03:47:29.934980

"""

# revision identifiers, used by Alembic.
revision = '2e8dfabeb9eb'
down_revision = '781ad80dcdc'

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.create_index('fk_lv_prev_inspections_restaurant_id_lv_restaurants', 'lv_prev_inspections', ['restaurant_id'])
    op.create_index('fk_yelp_reviews_business_id_yelp_businesses', 'yelp_reviews', ['business_id'])
    op.create_index('fk_yelp_reviews_user_id_yelp_users', 'yelp_reviews', ['user_id'])
    op.create_index('fk_yelp_checkins_business_id_yelp_businesses', 'yelp_checkins', ['business_id'])
    op.create_index('fk_yelp_tips_business_id_yelp_businesses', 'yelp_tips', ['business_id'])
    op.create_index('fk_yelp_tips_user_id_yelp_users', 'yelp_tips', ['user_id'])


def downgrade():
    op.drop_index('fk_yelp_tips_user_id_yelp_users', table_name='yelp_tips')
    op.drop_index('fk_yelp_tips_business_id_yelp_businesses', table_name='yelp_tips')
    op.drop_index('fk_yelp_checkins_business_id_yelp_businesses', table_name='yelp_checkins')
    op.drop_index('fk_yelp_reviews_user_id_yelp_users', table_name='yelp_reviews')
    op.drop_index('fk_yelp_reviews_business_id_yelp_businesses', table_name='yelp_reviews')
    op.drop_index('fk_lv_prev_inspections_restaurant_id_lv_restaurants', table_name='lv_prev_inspections')
