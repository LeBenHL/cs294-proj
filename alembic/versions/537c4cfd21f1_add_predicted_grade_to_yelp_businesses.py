"""Add predicted_A_grade to yelp_businesses

Revision ID: 537c4cfd21f1
Revises: 33897c71624
Create Date: 2014-11-29 11:05:39.155372

"""

# revision identifiers, used by Alembic.
revision = '537c4cfd21f1'
down_revision = '33897c71624'

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.add_column('yelp_businesses', sa.Column('predicted_A_grade', sa.Boolean))


def downgrade():
    op.drop_column('yelp_businesses', 'predicted_A_grade')
