import csv
from datetime import datetime
import simplejson as json

from flask.ext.script import Manager
from datasf_yelp import app, db
from datasf_yelp.models import LVRestaurant, LVPrevInspection, SFRestaurant, SFInspection, SFViolation, YelpBusiness, \
    YelpReview, YelpUser, YelpCheckin, YelpTip, YelpLVRestaurantSimilarity, YelpLVRestaurantEquivalent
from sqlalchemy.exc import IntegrityError

from Levenshtein import jaro, ratio

manager = Manager(app)

@manager.command
def load_sf_data():
    # Load Restaurant Data
    with open('data/datasf/businesses_plus.csv', 'rb') as csvfile:
        reader = csv.DictReader(csvfile)
        for row in reader:
            for key, value in row.iteritems():
                if not value:
                    row[key] = None
            if row['application_date']:
                date = row['application_date'].split('/')
                row['application_date'] = '%s-%s-%s' % (date[2], date[0], date[1])
            db.session.add(SFRestaurant(**row))
        db.session.commit()

    # Load Inspection Data
    with open('data/datasf/inspections_plus.csv', 'rb') as csvfile:
        reader = csv.DictReader(csvfile)
        for row in reader:
            for key, value in row.iteritems():
                if not value:
                    row[key] = None
            if row['date']:
                date = datetime.strptime(row['date'], '%Y%m%d')
                row['date'] = date.strftime('%Y-%m-%d')
            db.session.add(SFInspection(**row))
        db.session.commit()

    # Load Violation Data
    with open('data/datasf/violations_plus.csv', 'rb') as csvfile:
        reader = csv.DictReader(csvfile)
        for row in reader:
            for key, value in row.iteritems():
                if not value:
                    row[key] = None
            if row['date']:
                date = datetime.strptime(row['date'], '%Y%m%d')
                row['date'] = date.strftime('%Y-%m-%d')
            db.session.add(SFViolation(**row))
        db.session.commit()

@manager.command
def load_lv_data():
    """
    Loads Las Vegas Restaurant inspection grade data into the database from a json file
    """
    with open('data/scraping/southern_nevada_scores.json', 'rb') as f:
        restaurants = json.loads(f.readline())
        for restaurant in restaurants:
            prev_inspections = restaurant.pop('prev_insp')
            restaurant = LVRestaurant(**restaurant)
            db.session.add(restaurant)
            db.session.commit()

            for prev_inspection in prev_inspections:
                prev_inspection['restaurant_id'] = restaurant.id
                del prev_inspection['fixed_date']
                db.session.add(LVPrevInspection(**prev_inspection))
            db.session.commit()

@manager.command
@manager.option('-d', '--data', dest='data_location', help='Yelp Data Location')
def load_yelp_data(data_location=''):
    with open(data_location) as f:
        for line in f:
            try:
                contents = json.loads(line)
                json_type = contents.pop('type')
                if json_type == 'business':
                    db.session.add(YelpBusiness(**contents))
                elif json_type == 'review':
                    db.session.add(YelpReview(**contents))
                elif json_type == 'user':
                    del contents['friends']
                    db.session.add(YelpUser(**contents))
                elif json_type == 'checkin':
                    db.session.add(YelpCheckin(**contents))
                elif json_type == 'tip':
                    db.session.add(YelpTip(**contents))
                db.session.commit()
            except json.scanner.JSONDecodeError:
                print line

@manager.command
@manager.option('-n', '--num_restaurants', dest='num_restaurants', help='Number of Restaurants')
@manager.option('-r', '--reresolve', dest='reresolve', help='Reresolve')
def entity_resolve_sf_restaurants_with_yelp(num_restaurants=-1, reresolve=False):
    """
    Entity resolve NUM_RESTURANTS SF Restaurants with those on Yelp using the Yelp Search API and
    save the appropriate fields in the database. If reresolve is set True, we will entity resolve
    restaurants again for restaurants we already have Yelp Data saved for.
    """
    if not reresolve:
        restaurants = SFRestaurant.query.filter(SFRestaurant.yelp_id == None)
    else:
        restaurants = SFRestaurant.query

    if num_restaurants > 0:
        restaurants = restaurants.limit(num_restaurants)

    for i, restaurant in enumerate(restaurants):
        print "%d. Resolving: %s" % (i + 1, restaurant.name)
        yelp_equivalent, address_similarity, name_similarity, weighted_similarity = restaurant.find_yelp_equivalent()
        if yelp_equivalent:
            restaurant.yelp_id = yelp_equivalent['id']
            restaurant.yelp_name = yelp_equivalent['name']
            restaurant.yelp_url = yelp_equivalent['url']
            restaurant.yelp_rating = yelp_equivalent['rating']
            restaurant.yelp_address = "; ".join(yelp_equivalent['location']['display_address'])
            restaurant.address_similarity = address_similarity
            restaurant.name_similarity = name_similarity
            db.session.commit()

@manager.command
def calculate_yelp_lv_similarities():
    """
    Fill the Yelp/LV Restaurant similarity table by calculating the string similarity between the names and addresses of
    any pair of Yelp business and LV Restaurant that are within LAT_DELTA in latitude and LONG_DELTA in longitude.
    Lower values of the deltas will result in a more restrictive search space.
    """
    LAT_DELTA = 0.0015 # Approximately .07 miles
    LONG_DELTA = 0.0015 # Approximately .07 miles

    lv_restaurants = LVRestaurant.query

    for i, lv_restaurant in enumerate(lv_restaurants):
        lv_lat = lv_restaurant.latitude
        lv_long = lv_restaurant.longitude

        if not (lv_lat and lv_long):
            continue

        yelp_businesses = YelpBusiness.query.filter(YelpBusiness.state == 'NV',
                                                    YelpBusiness.latitude <= lv_lat + LAT_DELTA,
                                                    YelpBusiness.latitude >= lv_lat - LAT_DELTA,
                                                    YelpBusiness.longitude <= lv_long + LONG_DELTA,
                                                    YelpBusiness.longitude >= lv_long - LONG_DELTA
                                                    )
        print "%d/ %s - %d Yelp Businesses" % (i + 1, lv_restaurant.restaurant_name, yelp_businesses.count())

        for yelp_business in yelp_businesses:
            if not YelpLVRestaurantSimilarity.query.get((lv_restaurant.id, yelp_business.business_id)):
                yelp_address = get_yelp_business_street_address(yelp_business)

                if not (lv_restaurant.restaurant_name and yelp_business.name and lv_restaurant.address and yelp_address):
                    continue

                similarity = YelpLVRestaurantSimilarity(lv_restaurant_id=lv_restaurant.id, yelp_business_id=yelp_business.business_id)

                lv_name = lv_restaurant.restaurant_name.lower()
                yelp_name = yelp_business.name.lower()
                lv_address = lv_restaurant.address.lower()
                yelp_address = yelp_address.lower()

                similarity.name_ratio_score = ratio(lv_name, yelp_name)
                similarity.name_jaro_score = jaro(lv_name, yelp_name)
                similarity.address_ratio_score = ratio(lv_address, yelp_address)
                similarity.address_jaro_score = jaro(lv_address, yelp_address)
                similarity.latitude_delta = abs(lv_restaurant.latitude - yelp_business.latitude)
                similarity.longitude_delta = abs(lv_restaurant.longitude - yelp_business.longitude)

                try:
                    db.session.add(similarity)
                    db.session.commit()
                except IntegrityError:
                    db.session.rollback()

@manager.command
def fix_address_similarities():
    """
    A command for Ben to fix the address similiarities in the similarities table. Before, we were assuming the street address to be on the
    first row. This is not the case sometimes. We fix this by iterating through all yelp businesses that do not have a street address
    in the first row and recalculate the address similarities for them using our new get_yelp_business_street_address function.
    """
    yelp_businesses = YelpBusiness.query.filter(YelpBusiness.state == 'NV', YelpBusiness.full_address.op('regexp')('^[[:alpha:]]'))

    for i, yelp_business in enumerate(yelp_businesses):
        print "%d/ %s" % (i + 1, yelp_business.name)
        similarities = yelp_business.similarities
        for similarity in similarities:
            lv_restaurant = similarity.lv_restaurant

            yelp_address = get_yelp_business_street_address(yelp_business)

            if not (lv_restaurant.restaurant_name and yelp_business.name and lv_restaurant.address and yelp_address):
                continue

            lv_address = lv_restaurant.address.lower()
            yelp_address = yelp_address.lower()

            similarity.address_ratio_score = ratio(lv_address, yelp_address)
            similarity.address_jaro_score = jaro(lv_address, yelp_address)

            try:
                db.session.add(similarity)
                db.session.commit()
            except IntegrityError:
                db.session.rollback()

def get_yelp_business_street_address(yelp_business):
    """
    Attempt to get a yelp businesses street address by looking at the first two lines
    of a full address. In general, the street address can be found by looking at those lines
    and choosing the one that starts of with a digit (Some street no.)
    """
    addresses = yelp_business.full_address.split("\n")

    try:
        address_row_1 = addresses[0]
        address_row_1_starts_with_digit = address_row_1[0].isdigit()
    except IndexError:
        address_row_1 = None
        address_row_1_starts_with_digit = False

    try:
        address_row_2 = addresses[1]
        address_row_2_starts_with_digit = address_row_2[0].isdigit()
    except IndexError:
        address_row_2 = None
        address_row_2_starts_with_digit = False

    if address_row_1_starts_with_digit:
        return address_row_1
    elif address_row_2_starts_with_digit:
        return address_row_2
    else:
        return address_row_1

@manager.command
def manually_label_is_same_entity():
    """
    Command line tool to manually label entries in the similarity table. This manually labeling can eventually be used
    to train and test a binary classifier on determining if a lv restaurant/yelp business pair are of the same entity
    """

    similarities = YelpLVRestaurantSimilarity.query.filter(YelpLVRestaurantSimilarity.is_same_entity == None).order_by(db.func.rand()).all()

    for similarity in similarities:
        lv_restaurant = similarity.lv_restaurant
        yelp_business = similarity.yelp_business

        print "Name 1: %s" % lv_restaurant.restaurant_name
        print "Name 2: %s" % yelp_business.name
        print "Address 1: %s" % lv_restaurant.address
        print "Address 2: %s" % ",".join(yelp_business.full_address.split('\n'))
        print

        try:
            while True:
                is_same_entity = input('Is this is the same entity (2 for skip, 1 for yes, 0 for no)?: ')
                print

                if is_same_entity in [0, 1]:
                    similarity.is_same_entity = is_same_entity
                    db.session.add(similarity)
                    break
                elif is_same_entity == 2:
                    break

        except:
            db.session.commit()
            break

@manager.command
def manually_label_is_same_entity_2():
    """
    Command line tool to manually label entries in the similarity table. This manually labeling can eventually be used
    to train and test a binary classifier on determining if a lv restaurant/yelp business pair are of the same entity
    """

    lv_restaurants = LVRestaurant.query.order_by(db.func.rand())

    for lv_restaurant in lv_restaurants:
        similarities = lv_restaurant.similarities

        for i, similarity in enumerate(similarities):
            lv_restaurant = similarity.lv_restaurant
            yelp_business = similarity.yelp_business

            print "Yelp Business %d" % i
            print "Name 1: %s" % lv_restaurant.restaurant_name
            print "Name 2: %s" % yelp_business.name
            print "Address 1: %s" % lv_restaurant.address
            print "Address 2: %s" % ",".join(yelp_business.full_address.split('\n'))
            print

        if similarities:
            try:
                while True:
                    idx = input('Which index above is the same entity (-1 is there is None, -2 to skip)?: ')
                    print idx
                    print

                    if idx >= 0 and idx < len(similarities):
                        for i, similarity in enumerate(similarities):
                            if i == idx:
                                similarity.is_same_entity = True
                            else:
                                similarity.is_same_entity = False
                            db.session.add(similarity)
                        break
                    elif idx == -1:
                        for similarity in similarities:
                            similarity.is_same_entity = False
                            db.session.add(similarity)
                        break
                    elif idx == -2:
                        break
            except:
                db.session.commit()
                break

@manager.command
@manager.option('-d', '--data', dest='data_location', help='Yelp Data Location')
def load_yelp_lv_restaurants_equivalents(data_location=''):

    YelpLVRestaurantEquivalent.query.delete() # Clear the Whole Table

    with open(data_location, 'rb') as csvfile:
        reader = csv.DictReader(csvfile)
        for row in reader:
            lv_restaurant = LVRestaurant.query.get(row['lv_restaurant_id'])
            yelp_business = YelpBusiness.query.get(row['yelp_business_id'])

            lv_restaurant.yelp_businesses.append(yelp_business)

            db.session.commit()

@manager.command
@manager.option('-d', '--data', dest='data_location', help='Predictions Data Location')
def load_predictions(data_location=''):
    with open(data_location, 'rb') as csvfile:
        reader = csv.DictReader(csvfile)
        for row in reader:
            yelp_business = YelpBusiness.query.get(row['business_id'])
            if yelp_business:
                if row['grade'] == 'True':
                    predicted_A_grade = True
                elif row['grade'] == 'False':
                    predicted_A_grade = False
                else:
                    predicted_A_grade = None
                yelp_business.predicted_A_grade = predicted_A_grade

            db.session.commit()

if __name__ == "__main__":
    manager.run()